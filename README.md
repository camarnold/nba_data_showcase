# NBA_Data_Showcase

This repo has several notebooks about scraping data from https://www.basketball-reference.com/, cleaning it up a bit, exploring it, then doing some basic data science problems.